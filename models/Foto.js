var mongoose = require('mongoose'),
    extend = require('mongoose-schema-extend');

var FotoSchema = new mongoose.Schema({
    url: String,
    kamp: { type: mongoose.Schema.Types.ObjectId, ref: 'Kamp' }
});

mongoose.model('Foto', FotoSchema);
