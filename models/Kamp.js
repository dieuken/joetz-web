var mongoose = require('mongoose'),
    extend = require('mongoose-schema-extend');

var KampSchema = new mongoose.Schema({
    naam: String,
    omschrijving: String,
    startDatum: String,
    eindDatum: String,
    aantalDagen: String,
    aantalNachten: String,
    vervoer: String,
    formule: String,
    basisPrijs: String,
    bondPrijs: String,
    kortingen: String,
    inbegrepenInPrijs: String,
    minLeeftijd: String,
    maxLeeftijd: String,
    maxDeelnemers: String,
    contact: String,
    sfeerfoto: String,
    fotos: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Foto' }],
    inschrijvingen: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Inschrijving' }],
    beheerders: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Beheerder' }],
    monitoren: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Monitor' }],
    activiteiten: [{type: mongoose.Schema.Types.ObjectId, ref: 'Activiteit'}],
    adres: { type: mongoose.Schema.Types.ObjectId, ref: 'Adres' }
});

mongoose.model('Kamp', KampSchema);
