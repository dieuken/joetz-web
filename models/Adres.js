var mongoose = require('mongoose'),
    extend = require('mongoose-schema-extend');


var AdresSchema = new mongoose.Schema({
  straat: String,
  huisnummer: String,
  bus: String,
  gemeente: String,
  postcode: String,
  beheerder: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Beheerder' }],
  monitoren: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Monitor' }],
  leden: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Lid' }],
  contactpersonen: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Contactpersoon' }],
  kampen: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Kamp' }]
});

mongoose.model('Adres', AdresSchema);
