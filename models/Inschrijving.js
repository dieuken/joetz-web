var mongoose = require('mongoose'),
    extend = require('mongoose-schema-extend');

var InschrijvingSchema = new mongoose.Schema({
    extraInformatie: String,
    goedgekeurd: String,
    betaald: String,
    kamp: { type: mongoose.Schema.Types.ObjectId, ref: 'Kamp' },
    lid: { type: mongoose.Schema.Types.ObjectId, ref: 'Lid' }
});

mongoose.model('Inschrijving', InschrijvingSchema);
