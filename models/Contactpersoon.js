var mongoose = require('mongoose'),
    extend = require('mongoose-schema-extend');

var ContactpersoonSchema = new mongoose.Schema({
    naam: String,
    voornaam: String,
    rijksregisternummer: String,
    telefoonnummer: String,
    email: String,
    aansluitnummer: String,
    betalend: String,
    ouder: String,
    leden: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Lid' }],
    adres: { type: mongoose.Schema.Types.ObjectId, ref: 'Adres' }
});

mongoose.model('Contactpersoon', ContactpersoonSchema);
