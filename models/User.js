var mongoose = require('mongoose'),
    extend = require('mongoose-schema-extend');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var UserSchema = new mongoose.Schema({
	email: {type: String, lowercase: true, unique:true},
	hash: String,
	salt: String,
  naam: String,
  voornaam: String,
  geactiveerd: String,
  rijksregisternummer: String
}, { collection : 'users', discriminatorKey : '_type' });

UserSchema.methods.setPassword = function(password){
	this.salt = crypto.randomBytes(16).toString('hex');
	this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

UserSchema.methods.validPassword = function(password){
	var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
	return hash === this.hash;
};

UserSchema.methods.generateJWT = function(){
	var today = new Date();
	var exp = new Date(today);
	exp.setDate(today.getDate() + 60);

	return jwt.sign({
	_id: this._id,
  _type: this._type,
	info: this.voornaam + " " + this.naam,
	exp: parseInt(exp.getTime() / 1000),
	}, 'SECRET');
};

mongoose.model('User', UserSchema);
console.log('UserSchema made');

var BeheerderSchema = UserSchema.extend({
    activiteiten: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Activiteit' }],
    adres: { type: mongoose.Schema.Types.ObjectId, ref: 'Adres' },
    kampen: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Kamp' }]
});

mongoose.model('Beheerder', BeheerderSchema);
console.log('BeheerderSchema made');

var LidSchema = UserSchema.extend({
    geboorteDatum: String,
    codeGerechtigde: String,
    adres: { type: mongoose.Schema.Types.ObjectId, ref: 'Adres' },
    inschrijvingen: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Inschrijving' }],
    contactpersonen: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Contactpersoon' }]
});

mongoose.model('Lid', LidSchema);
console.log('LidSchema made');

var MonitorSchema = UserSchema.extend({
    activiteiten: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Activiteit' }],
    adres: { type: mongoose.Schema.Types.ObjectId, ref: 'Adres' },
    kampen: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Kamp' }]
});

mongoose.model('Monitor', MonitorSchema);
console.log('MonitorSchema made');
