var mongoose = require('mongoose');
var extend = require('mongoose-schema-extend');

var ActiviteitSchema = new mongoose.Schema({
    naam: String,
    locatie: String,
    heleDag: String,
    begin: String,
    einde: String,
    beheerders: [{type: mongoose.Schema.Types.ObjectId, ref: 'Beheerder'}],
    monitoren: [{type: mongoose.Schema.Types.ObjectId, ref: 'Monitor'}],
    kamp: {type: mongoose.Schema.Types.ObjectId, ref: 'Kamp'}
});

mongoose.model('Activiteit', ActiviteitSchema);
