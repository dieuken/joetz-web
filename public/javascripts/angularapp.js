var app = angular.module('Joetz', ['ui.router','angular-flexslider']);
app.config(['$stateProvider', '$urlRouterProvider',
function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: '/home.html',
      controller: 'MainCtrl',
      resolve: {
        postPromise: ['kampen', function(kampen){
          return kampen.getAll();
        }]
      }
    })

    .state('login', {
      url: '/login',
      templateUrl: '/login.html',
      controller: 'AuthCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(auth.isLoggedIn()){
          $state.go('home');
        }
      }]
    })
    .state('register', {
      url: '/register',
      templateUrl: '/register.html',
      controller: 'AuthCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(auth.isLoggedIn()){
          $state.go('home');
        }
      }]
    })

    .state('registerMonitor', {
      url: '/registerMonitor',
      templateUrl: '/registerMonitor.html',
      controller: 'AuthCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(!auth.isLoggedIn() || auth.islid()){
          $state.go('home');
        }
      }]
    })

    .state('account', {
      url: '/account',
      templateUrl: '/account.html',
      controller: 'AccountCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(!auth.isLoggedIn()){
          $state.go('home');
        }
      }],
      resolve: {
        user: ['auth', function(auth) {
          return auth.getUser();
        }]
      }
    })

    .state('kamptoevoegen', {
      url: '/kampToevoegen',
      templateUrl: '/kamptoevoegen.html',
      controller: 'KampToevoegenCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(!auth.isbeheerder()){
          $state.go('home');
        }
      }]
    })

    .state('contact', {
      url: '/contact',
      templateUrl: '/contact.html',
      //hier moet een onEnter komen zoals bij login maar dan met zelf gemaakte functie om te kijken of user een beheerder is
    })

    .state('kampen', {
      url: '/kampen/:id',
      templateUrl: '/kamp.html',
      controller: 'KampenCtrl',
      resolve: {
        kamp: ['$stateParams', 'kampen', function($stateParams, kampen) {
          return kampen.get($stateParams.id);
        }]
      }
    })

    .state('kampaanpassen', {
      url: '/kampaanpassen/:id',
      templateUrl: '/kampaanpassen.html',
      controller: 'KampAanpassenCtrl',
      resolve: {
        kamp: ['$stateParams', 'kampen', function($stateParams, kampen) {
          return kampen.get($stateParams.id);
        }]
      }
    })

    .state('contactpersoonToevoegen', {
      url: '/contactpersoonToevoegen',
      templateUrl: '/contactpersoontoevoegen.html',
      controller: 'CPToevoegenCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(!auth.islid()){
          $state.go('home');
        }
      }]
    })

    .state('contactpersoon', {
      url: '/contactpersoon/:id',
      templateUrl: '/contactpersoon.html',
      controller: 'ContactpersoonCtrl',
      resolve: {
        contactpersoon: ['$stateParams', 'auth', function($stateParams, auth) {
          return auth.getContactpersoon($stateParams.id);
        }]
      }
    })

    .state('contactpersoonAanpassen', {
      url: '/contactpersoonAanpassen/:id',
      templateUrl: '/contactpersoonaanpassen.html',
      controller: 'CPAanpassenCtrl',
      resolve: {
        contactpersoon: ['$stateParams', 'auth', function($stateParams, auth) {
          return auth.getContactpersoon($stateParams.id);
        }]
      }
    })

    .state('activiteitToevoegen', {
      url: '/kamp/:id/activiteittoevoegen',
      templateUrl: '/activiteittoevoegen.html',
      controller: 'activiteitToevoegenCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(auth.islid()){
          $state.go('home');
        }
      }],
      resolve: {
        kamp: ['$stateParams', 'kampen', function($stateParams, kampen) {
          return kampen.get($stateParams.id);
        }]
      }
    })

    .state('activiteit', {
      url: '/activiteit/:id',
      templateUrl: '/activiteit.html',
      controller: 'ActiviteitCtrl',
      resolve: {
        activiteit: ['$stateParams', 'kampen', function($stateParams, kampen) {
          return kampen.getActiviteit($stateParams.id);
        }]
      }
    })

    .state('activiteitAanpassen', {
      url: '/activiteitAanpassen/:id',
      templateUrl: '/activiteitaanpassen.html',
      controller: 'activiteitAanpassenCtrl',
      resolve: {
        activiteit: ['$stateParams', 'kampen', function($stateParams, kampen) {
          return kampen.getActiviteit($stateParams.id);
        }]
      }
    })

    .state('mijnActiviteiten', {
      url: '/mijnActiviteiten',
      templateUrl: '/mijnactiviteiten.html',
      controller: 'mijnActiviteitenCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(!auth.ismoderator()){
          $state.go('home');
        }
      }],
      resolve: {
        user: ['auth', function(auth) {
          return auth.getUser();
        }]
      }
    })
    .state('inschrijving', {
      url: '/kampen/:id/inschrijving',
      templateUrl: '/inschrijving.html',
      controller: 'inschrijvingCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(!auth.islid()){
          $state.go('home');
        }
      }],
      resolve: {
        kamp: ['$stateParams', 'kampen', function($stateParams, kampen) {
          return kampen.get($stateParams.id);
        }]
      }
    })

    .state('mijnInschrijvingen', {
      url: '/mijnInschrijvingen',
      templateUrl: '/mijninschrijvingen.html',
      controller: 'mijnInschrijvingenCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(!auth.islid()){
          $state.go('home');
        }
      }],
      resolve: {
        inschrijvingen: ['auth', function(auth) {
          var user = auth.currentUser();
          return auth.inschrijvingenperlid(user._id).then(function(res){
            return res;
          });
        }]
      }
    })

    .state('kampInschrijvingen', {
      url: '/kampInschrijvingen/:id',
      templateUrl: '/kampinschrijvingen.html',
      controller: 'kampInschrijvingenCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(auth.islid()){
          $state.go('home');
        }
      }],
      resolve: {
        inschrijvingen: ['$stateParams', 'kampen', function($stateParams, kampen) {
          return kampen.inschrijvingenperkamp($stateParams.id).then(function(res){
            return res;
          });
        }]
      }
    })

    .state('kampInschrijving', {
      url: '/kampInschrijving/:id',
      templateUrl: '/kampinschrijving.html',
      controller: 'kampInschrijvingCtrl',
      onEnter: ['$state', 'auth', function($state, auth){
        if(auth.islid()){
          $state.go('home');
        }
      }],
      resolve: {
        inschrijving: ['$stateParams', 'auth', function($stateParams, auth) {
          return auth.getInschrijving($stateParams.id).then(function(res){
            return res;
          });
        }]
      }
    })
  $urlRouterProvider.otherwise('home');
}]);

app.factory('kampen', ['$http', 'auth', 'multipartForm', function($http, auth, multipartForm){
  var o = {
    kampen: []
  };

  o.getAll = function() {
    return $http.get('/kampen').success(function(data){
      angular.copy(data, o.kampen);
    });
  };

  o.get = function(id) {
    return $http.get('/kampen/' + id).then(function(res){
      return res.data;
    });
  };

  o.create = function(data) {

    var uploadUrl = '/kampen';
    var fd = new FormData();
    for(var key in data)
      fd.append(key, data[key]);
    return $http.post(uploadUrl, fd, {
        trasnformRequest: angular.identity,
        //angular serializeert de gegevens die we proberen uploaden maar omdat we met files werken willen we dit niet.
        headers: {'Content-Type': undefined, Authorization: 'Bearer '+auth.getToken()}
        //server weet welke content er verwacht wordt dus het is niet nodig voor deze te specifieëren
    }).then(function(res){
      return res;
    });
  };

  o.aanpassen = function(data) {
    var uploadUrl = '/kampen/'+data._id;
    var fd = new FormData();
    for(var key in data)
      fd.append(key, data[key]);
    return $http.put(uploadUrl, fd, {
        trasnformRequest: angular.identity,
        //angular serializeert de gegevens die we proberen uploaden maar omdat we met files werken willen we dit niet.
        headers: {'Content-Type': undefined, Authorization: 'Bearer '+auth.getToken()}
        //server weet welke content er verwacht wordt dus het is niet nodig voor deze te specifieëren
    }).then(function(res){
      return res;
    });
  };

  o.addAdres = function(id, adres) {
    return $http.post('/kampen/' + id + '/adres', adres);
  };
/*
  o.create = function(kamp) {
    return $http.post('/kampen', post, {
      headers: {Authorization: 'Bearer '+auth.getToken()}
    }).success(function(data){
      o.kampen.push(data);
    });
  };
*/
  o.adresaanpassen = function(adres){
      return $http.put('/adres', adres);
  };

  o.addFotos = function(id, data){
    //return $http.post('/kampen/' + id + '/fotos', fotos);
    var uploadUrl = '/kampen/' + id + '/fotos', data;
    var fd = new FormData();
    for(var key in data)
      fd.append(key, data[key]);
    return $http.post(uploadUrl, fd, {
        trasnformRequest: angular.identity,
        //angular serializeert de gegevens die we proberen uploaden maar omdat we met files werken willen we dit niet.
        headers: {'Content-Type': undefined, Authorization: 'Bearer '+auth.getToken()}
        //server weet welke content er verwacht wordt dus het is niet nodig voor deze te specifieëren
    });
  };

  o.addActiviteit = function(id, data){
    return $http.post('/monitoren/' + id + '/activiteit', data);
  };

  o.activiteitAanpassen = function(data){
    return $http.put('/activiteit/' + data._id, data);
  };

  o.getActiviteit = function(id){
    return $http.get('/activiteit/' + id).then(function(res){
      return res.data;
    });
  };

  o.inschrijvingenperkamp = function(id){
    return $http.get('/inschrijvingen/kamp/' + id).then(function(res){
      return res.data;
    });
  };

  return o;
}]);


app.factory('auth', ['$http', '$window', '$state', function($http, $window, $state){
   var auth = {};

  auth.saveToken = function (token){

    $window.localStorage['test-token'] = token;
  };

  auth.getToken = function (){
    return $window.localStorage['test-token'];
  };

  auth.isLoggedIn = function(){
    var token = auth.getToken();
    if(token){
      var payload = JSON.parse($window.atob(token.split('.')[1]));

      return payload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  };

  auth.currentUser = function(){
    if(auth.isLoggedIn()){
        var token = auth.getToken();
        var payload = JSON.parse($window.atob(token.split('.')[1]));
        return payload;
    }
  };

  auth.register = function(user){
    return $http.post('/registerLid', user).success(function(data){
      auth.saveToken(data.token);
    });
  };

  auth.registerMonitor = function(user){
    return $http.post('/registerMonitor', user).success(function(data){
      return data;
    });
  };

  auth.logIn = function(user){
    return $http.post('/login', user).success(function(data){
      auth.saveToken(data.token);
    });
  };

  auth.logOut = function(){
    $window.localStorage.removeItem('test-token');
    console.log('test');
    $state.go('home');
  };

  auth.usernaam = function(){
    var user = auth.currentUser();
    if(user){
      return user.info;
    }
  };

  auth.addAdresLid = function(id, adres) {
    return $http.post('/leden/' + id + '/adres', adres);
  };

  auth.addAdresMonitor = function(id, adres) {
    return $http.post('/Monitoren/' + id + '/adres', adres);
  };

  auth.getUser = function(){
    var user = auth.currentUser();
    if(user._type == "Lid"){
        return $http.get('/leden/' + user._id).then(function(res){
          return res.data;
        });
    }
    if(user._type == "Beheerder"){
      return $http.get('/beheerders/' + user._id).then(function(res){
        return res.data;
      });
    }
    if(user._type == "Monitor"){
      return $http.get('/monitoren/' + user._id).then(function(res){
        return res.data;
      });
    }
  };

  auth.getContactpersoon = function(id){
    return $http.get('/contactpersoon/' + id).then(function(res){
      return res.data;
    });
  };

  auth.addContactPersoon = function(id, contactpersoon){
    return $http.post('/leden/' + id + '/contactpersoon', contactpersoon);
  };

  auth.addCPAdres = function(id, adres){
    return $http.post('/contactpersoon/' + id + '/adres', adres);
  };

  auth.contactpersoonUpdate = function(id, contactpersoon){
    return $http.put('/leden/'+ id +'/contactpersoon', contactpersoon);
  };

  auth.userupdate = function(user){
    if(user._type == "Lid"){
        return $http.put('/lidaanpassen', user).then(function(res){
          $state.go('account');
        });
    }
    if(user._type == "Beheerder"){
      return $http.put('/beheerderaanpassen', user).then(function(res){
        $state.go('account');
      });
    }
    if(user._type == "Moderator"){
      return $http.put('/monitoraanpassen', user).then(function(res){
        $state.go('account');
      });
    }
  };

  auth.addKamp = function(id, data){
    return $http.put('/monitoren/'+ id +'/kamp', data);
  };

  auth.isbeheerder = function(){
    if(auth.isLoggedIn()){
      var user = auth.currentUser();
      if(user._type == "Beheerder"){
       return true;
       }
        else{
          return false;
        }
      }
      else{
        return false;
      }
    };

  auth.ismoderator = function(){
    if(auth.isLoggedIn()){
      var user = auth.currentUser();
      if(user._type == "Monitor"){
       return true;
       }
        else{
          return false;
        }
      }
      else{
        return false;
      }
    };

  auth.islid = function(){
    if(auth.isLoggedIn()){
      var user = auth.currentUser();
      if(user._type == "Lid"){
       return true;
       }
        else{
          return false;
        }
      }
      else{
        return false;
      }
    };

  auth.inschrijving = function(id, data){
    return $http.post('/leden/'+ id +'/inschrijven', data);
  };

  auth.inschrijvingenperlid = function(id){
    return $http.get('/inschrijvingen/lid/' + id).then(function(res){
      return res.data;
    });
  };

  auth.getInschrijving = function(id){
    return $http.get('/inschrijving/' + id).then(function(res){
      return res.data;
    });
  };

  auth.putInschrijving = function(data){
    return $http.put('/inschrijvingen/'+ data._id, data);
  };

  return auth;
}]);




app.controller('MainCtrl', [
'$scope',
'kampen',
'auth',
function($scope, kampen, auth){
$scope.kampen = kampen.kampen;
$scope.LoggedIn = auth.isLoggedIn;
}
]);



app.controller('KampenCtrl', [
'$scope',
'kampen',
'kamp',
'auth',
function($scope, kampen, kamp, auth){
  $scope.kamp = kamp;
  $scope.fotos = $scope.kamp.fotos;
  $scope.activiteiten = $scope.kamp.activiteiten;
  console.log($scope.activiteiten);
  $scope.isbeheerder = auth.isbeheerder;
  $scope.ismoderator = auth.ismoderator;
  $scope.islid = auth.islid;
  $scope.isLoggedIn = auth.isLoggedIn;
  $scope.moderator = function(){
    var user = auth.currentUser();
    auth.addKamp(user._id, $scope.kamp);
  }
}]);

app.controller('KampAanpassenCtrl', [
'$scope',
'$state',
'kampen',
'kamp',
'auth',
function($scope, $state, kampen, kamp, auth){
  $scope.kamp = kamp;
  $scope.fotos = $scope.kamp.fotos;

  $scope.isLoggedIn = auth.isLoggedIn;
  $scope.adres = $scope.kamp.adres;
  $scope.kamp.startDatum = new Date($scope.kamp.startDatum);
  $scope.kamp.eindDatum = new Date($scope.kamp.eindDatum);

  $scope.aanpassen = function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    today = new Date(mm+'/'+dd+'/'+yyyy);
    console.log(today);

    var startDatum = new Date($scope.kamp.startDatum);
    var startDag = startDatum.getDate();
    var startMaand = startDatum.getMonth() + 1;
    var startJaar = startDatum.getFullYear();
    startDatum = new Date(startMaand + '/' + startDag + '/' + startJaar);
    console.log(startDatum);

    var eindDatum = new Date($scope.kamp.eindDatum);
    var eindDag = eindDatum.getDate();
    var eindMaand = eindDatum.getMonth() + 1;
    var eindJaar = eindDatum.getFullYear();
    eindDatum = new Date(eindMaand + '/' + eindDag + '/' + eindJaar);
    console.log(eindDatum);

    if(startDatum < eindDatum && startDatum >= today && eindDatum > today)
    {
      $scope.kamp.startDatum = startMaand + '/' + startDag + '/' + startJaar;
      $scope.kamp.eindDatum = eindMaand + '/' + eindDag + '/' + eindJaar;
      console.log($scope.adres);
      $scope.kamp.adres = $scope.adres;
      console.log($scope.kamp);

      kampen.aanpassen($scope.kamp).then(function(res){
        kampen.adresaanpassen($scope.adres).then(function(res){
          if($scope.fotos[0] != $scope.kamp.fotos[0])
          {
            kampen.addFotos($scope.kamp._id, $scope.fotos).then(function(){
                $state.go('home');
              });
          }
          else {
            $state.go('home');
          }
        });
      });
    }
    else
      $scope.error = true;
      console.log('datum error');
    }
  }
]);

app.controller('KampToevoegenCtrl', [
'$scope',
'$state',
'kampen',
'auth',
'multipartForm',
function($scope, $state, kampen, auth, multipartForm){
  $scope.kampen = kampen.kampen;
  $scope.LoggedIn = auth.isLoggedIn;
  $scope.kamp = {};
  $scope.adres = {};
  $scope.fotos = [];
  $scope.error = false;
  $scope.addKamp = function(){

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    today = new Date(mm+'/'+dd+'/'+yyyy);
    console.log(today);

    var startDatum = $scope.kamp.startDatum;
    var startDag = startDatum.getDate();
    var startMaand = startDatum.getMonth() + 1;
    var startJaar = startDatum.getFullYear();
    startDatum = new Date(startMaand + '/' + startDag + '/' + startJaar);
    console.log(startDatum);

    var eindDatum = $scope.kamp.eindDatum;
    var eindDag = eindDatum.getDate();
    var eindMaand = eindDatum.getMonth() + 1;
    var eindJaar = eindDatum.getFullYear();
    eindDatum = new Date(eindMaand + '/' + eindDag + '/' + eindJaar);
    console.log(eindDatum);

    if(startDatum < eindDatum && startDatum >= today && eindDatum > today)
    {
      $scope.kamp.startDatum = startMaand + '/' + startDag + '/' + startJaar;
      $scope.kamp.eindDatum = eindMaand + '/' + eindDag + '/' + eindJaar;
      console.log($scope.kamp);


      kampen.create($scope.kamp).then(function(res){
        $scope.kamp = res.data;
        kampen.addAdres($scope.kamp._id, $scope.adres).then(function(){
            kampen.addFotos($scope.kamp._id, $scope.fotos).then(function(){
                $state.go('home');
              });
        });
      });

    }
    else
      $scope.error = true;
      console.log('datum error');
    }
  }
]);

app.controller('CPToevoegenCtrl', [
'$scope',
'$state',
'auth',
function($scope, $state, auth){
  $scope.contactpersoon = {};
  $scope.adres = {};
  $scope.opslaan = function(){
    var user = auth.currentUser();
    auth.addContactPersoon(user._id, $scope.contactpersoon).then(function(res){
      auth.addCPAdres(res.data._id, $scope.adres).then(function(res){
          $state.go('account');
      });
    });
    }
  }
]);

app.controller('ContactpersoonCtrl', [
  '$scope',
  'contactpersoon',
  'auth',
  function($scope, contactpersoon, auth){
    $scope.contactpersoon = contactpersoon;
    if($scope.contactpersoon.betalend){
      $scope.contactpersoon.betalend = 'ja';
    }
    else{
      $scope.contactpersoon.betalend = 'neen';
    }
    if($scope.contactpersoon.ouder){
      $scope.contactpersoon.ouder = 'ja';
    }
    else{
      $scope.contactpersoon.ouder = 'neen';
    }
  }
]);

app.controller('CPAanpassenCtrl', [
'$scope',
'$state',
'auth',
'contactpersoon',
function($scope, $state, auth, contactpersoon){
    $scope.contactpersoon = contactpersoon;
    $scope.adres = $scope.contactpersoon.adres;

    $scope.opslaan = function(){
      console.log($scope.contactpersoon);
      var user = auth.currentUser();
      auth.contactpersoonUpdate(user._id, $scope.contactpersoon).then(function(res){
          window.location = "#/contactpersoon/" + $scope.contactpersoon._id;
      });
    }
}
]);

app.controller('activiteitToevoegenCtrl', [
'$scope',
'$state',
'auth',
'kampen',
'kamp',
function($scope, $state, auth, kampen, kamp){
  $scope.activiteit = {};
  $scope.kamp = kamp;
  $scope.opslaan = function(){
      $scope.activiteit.kamp = $scope.kamp;
      var user = auth.currentUser();
      kampen.addActiviteit(user._id, $scope.activiteit).then(function(){
        window.location = "#/kampen/" + $scope.kamp._id;
      });
    }
  }
]);

app.controller('ActiviteitCtrl', [
  '$scope',
  'auth',
  'activiteit',
  function($scope, auth, activiteit){
    console.log(activiteit);
    $scope.activiteit = activiteit;
  }
]);

app.controller('mijnActiviteitenCtrl', [
'$scope',
'$state',
'auth',
'user',
function($scope, $state, auth, user){
    $scope.activiteiten = user.activiteiten;
    $scope.go = function(data){
      var id = data._id;
      window.location = "#/activiteit/" + id;
    }
  }
]);

app.controller('mijnInschrijvingenCtrl', [
'$scope',
'$state',
'auth',
'kampen',
'inschrijvingen',
function($scope, $state, auth, kampen, inschrijvingen){
    $scope.inschrijvingen = inschrijvingen;
    console.log($scope.inschrijvingen);

  }
]);

app.controller('kampInschrijvingCtrl', [
'$scope',
'$state',
'auth',
'kampen',
'inschrijving',
function($scope, $state, auth, kampen, inschrijving){
    $scope.isbeheerder = auth.isbeheerder;
    $scope.inschrijving = inschrijving;
    console.log($scope.inschrijving);
    $scope.goedgekeurd = function(){
      $scope.inschrijving.goedgekeurd = 'ja';
      auth.putInschrijving($scope.inschrijving);
    }
    $scope.betaald = function(){
      $scope.inschrijving.betaald = 'ja';
      auth.putInschrijving($scope.inschrijving);
    }
  }
]);

app.controller('kampInschrijvingenCtrl', [
'$scope',
'$state',
'auth',
'kampen',
'inschrijvingen',
function($scope, $state, auth, kampen, inschrijvingen){
    $scope.inschrijvingen = inschrijvingen;
    $scope.go = function(data){
      window.location = "#/kampInschrijving/" + data._id;
    }
  }
]);

app.controller('activiteitAanpassenCtrl', [
'$scope',
'$state',
'kampen',
'activiteit',
function($scope, $state, kampen, activiteit){
    $scope.activiteit = activiteit;
    $scope.opslaan = function(){
      kampen.activiteitAanpassen($scope.activiteit).then(function(){
        window.location = "#/activiteit/" + $scope.activiteit._id;
      });
    }
  }
]);

app.controller('inschrijvingCtrl', [
  '$scope',
  'auth',
  'kamp',
  function($scope, auth, kamp){
    $scope.kamp = kamp;
    $scope.inschrijving = {};
    $scope.opslaan = function(){
      var user = auth.currentUser();
      $scope.inschrijving.kamp = $scope.kamp;
      auth.inschrijving(user._id, $scope.inschrijving).then(function(){
        window.location = "#/activiteit/" + $scope.activiteit._id;
      });
    }
  }
]);

app.controller('AccountCtrl', [
  '$scope',
  '$state',
  'auth',
  'user',
  function($scope, $state, auth, user){
    $scope.user = user;
    $scope.aanpassen = true;
    $scope.adres = $scope.user.adres;
    $scope.contactpersonen = $scope.user.contactpersonen;
    $scope.islid = auth.islid;
    $scope.btnopslaan = {'display': 'none'};
    $scope.btnaanpassen = {'display': 'block'};
    var geboorteDatum = new Date($scope.user.geboorteDatum);
    var geboorteDag = geboorteDatum.getDate();
    var geboorteMaand = geboorteDatum.getMonth() + 1;
    var geboorteJaar = geboorteDatum.getFullYear();
    $scope.user.geboorteDatum = new Date(geboorteJaar + '/' + geboorteMaand + '/' + geboorteDag);
    $scope.pasaan = function(){
      $scope.aanpassen = false;
    };

    $scope.opslaan = function(){
      $scope.user.adres = $scope.adres;
      auth.userupdate($scope.user);
    };

    $scope.heeftCP = function(){
        if($scope.contactpersonen){
            return false;
        }
        else{
            return true;
        }
    };

   $scope.go = function(data){
      window.location = "#/contactpersoon/" + data._id;
   };

    $scope.contactpersoon = function(){
      $state.go('contactpersoonToevoegen')
    }
  }
]);

app.controller('AuthCtrl', [
'$scope',
'$state',
'auth',
function($scope, $state, auth){
  $scope.user = {};
  $scope.adres = {};

  $scope.register = function(){
    var geboorteDatum = new Date($scope.user.geboorteDatum);
    var geboorteDag = geboorteDatum.getDate();
    var geboorteMaand = geboorteDatum.getMonth() + 1;
    var geboorteJaar = geboorteDatum.getFullYear();
    $scope.user.geboorteDatum = geboorteMaand + '/' + geboorteDag + '/' + geboorteJaar;
    auth.register($scope.user).error(function(error){
      $scope.error = error;
    }).then(function(){
      var user = auth.currentUser();
      auth.addAdresLid(user._id, $scope.adres).error(function(error){
        $scope.error = error;
      }).then(function(){
      $state.go('home');
      });
    });
  };

  $scope.registerMonitor = function(){
      auth.registerMonitor($scope.user).error(function(error){
        $scope.error = error;
      }).then(function(data){
          console.log(data.data);
          auth.addAdresMonitor(data.data._id, $scope.adres).error(function(error){
            $scope.error = error;
          }).then(function(){
          $state.go('home');
          });
      });
  };

  $scope.logIn = function(){
    auth.logIn($scope.user).error(function(error){
      $scope.error = error;
    }).then(function(){
        $state.go('home');
    });
  };
}]);



app.controller('NavCtrl', [
'$scope',
'auth',
function($scope, auth){
  $scope.LoggedIn = auth.isLoggedIn;
  $scope.usernaam = auth.usernaam;
  $scope.logOut = auth.logOut;
  $scope.isbeheerder = auth.isbeheerder;
  $scope.ismoderator = auth.ismoderator;
  $scope.islid = auth.islid;
  console.log($scope.ismoderator());
}]);

app.directive('fileModel', ['$parse', function($parse){
    return{
        restrict: 'A',
        link: function(scope, element, attrs){
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.directive('fileModels', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModels);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;
            element.bind('change', function () {
                var values = [];
                angular.forEach(element[0].files, function (item) {
                    var file = item;
                    values.push(file);
                });
                scope.$apply(function () {
                    if (isMultiple) {
                        modelSetter(scope, values);
                    } else {
                      modelSetter(scope, values[0]);
                }
            });
        });
    }
};
}]);

app.service('multipartForm', ['$http', function($http){
    this.post = function(uploadUrl, data){
        var fd = new FormData();
        for(var key in data)
          fd.append(key, data[key]);
        return $http.post(uploadUrl, fd, {
            trasnformRequest: angular.identity,
            //angular serializeert de gegevens die we proberen uploaden maar omdat we met files werken willen we dit niet.
            headers: {'Content-Type': undefined}
            //server weet welke content er verwacht wordt dus het is niet nodig voor deze te specifieëren
        }).then(function(res){
          return res;
        });
    }
}]);

app.directive("passwordVerify", function() {
   return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ctrl) {
        scope.$watch(function() {
            var combined;

            if (scope.passwordVerify || ctrl.$viewValue) {
               combined = scope.passwordVerify + '_' + ctrl.$viewValue;
            }
            return combined;
        }, function(value) {
            if (value) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var origin = scope.passwordVerify;
                    if (origin !== viewValue) {
                        ctrl.$setValidity("passwordVerify", false);
                        return undefined;
                    } else {
                        ctrl.$setValidity("passwordVerify", true);
                        return viewValue;
                    }
                });
            }
        });
     }
   };
});
