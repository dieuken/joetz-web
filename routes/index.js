var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
//var extend = require('mongoose-schema-extend');
var Activiteit = mongoose.model('Activiteit');
var Adres = mongoose.model('Adres');
var Beheerder = mongoose.model('Beheerder');
var Contactpersoon = mongoose.model('Contactpersoon');
var Foto = mongoose.model('Foto');
var Inschrijving = mongoose.model('Inschrijving');
var Kamp = mongoose.model('Kamp');
var Lid = mongoose.model('Lid');
var Monitor = mongoose.model('Monitor');
var User = mongoose.model('User');
var passport = require('passport');
var jwt = require('express-jwt');
var auth = jwt({secret: 'SECRET', userProperty: 'payload'});
var fs = require('fs');


router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//kampen

router.get('/kampen', function(req, res, next) {
  Kamp.find(function(err, kampen){
    if(err){ return next(err); }
    res.json(kampen);
  });
});

router.param('kamp', function(req, res, next, id) {
  var query = Kamp.findById(id);

  query.exec(function (err, kamp){
    if (err) { return next(err); }
    if (!kamp) { return next(new Error("can't find kamp")); }

    req.kamp = kamp;
    return next();
  });
});

router.get('/kampen/:kamp', function(req, res, next){
    var id = req.kamp._id;
    Kamp.findOne({ _id: id })
    .populate([{path:'adres'}, {path:'inschrijvingen'}, {path:'beheerder'}, {path: 'monitors'}, {path: 'fotos'}, {path:'activiteiten'}])
    .exec (function (err, kamp) {
    if (err){
      console.log(err);
    }
    else{
      req.kamp = kamp;
      res.json(req.kamp);
    }
  })

});

router.post('/kampen',function(req,res,next){
	var kamp = new Kamp(req.body);
  var sfeerfoto = req.files.sfeerfoto.path;
  sfeerfoto = sfeerfoto.replace("public", ".");
  kamp.sfeerfoto = sfeerfoto;
  kamp.save(function(err, kamp){
    if(err){ return next(err); }
		res.json(kamp);
  });
});

router.post('/kampen/:kamp/adres', function(req, res, next) {
  var adres = new Adres(req.body);
  adres.kampen.push(req.kamp);

  adres.save(function(err, adres){
    if(err){ return next(err); }

    req.kamp.adres = adres;
    req.kamp.save(function(err, kamp) {
      if(err){ return next(err); }

      res.json(kamp);
    });
  });
});

router.post('/kampen/:kamp/fotos', function(req, res, next) {
  var fotos = req.files;
  //for is om IE8 of lager te ondersteunen
  var length= 0;
  for(var key in fotos) {
      if(fotos.hasOwnProperty(key)){
          length++;
      }
  }
  var i;
  var urlfotos = [];
  kamp = req.kamp;
  for(i = 0; i < length; i++){
    var foto = new Foto();
    var url = fotos[i].path
    foto.url = url.replace('public', '.');
    urlfotos.push(foto);
    foto.save(function(err, foto) {
        if(err){ return next(err); }
    });/*
    req.kamp.fotos.push(foto)
    req.kamp.save(function(err, kamp){
      if(err){ return next(err); }
    });*/
  };

  req.kamp.fotos = urlfotos;
  req.kamp.save(function(err, kamp){
    if(err){ return next(err); }
    res.json(kamp);
  });

});

router.put('/kampen/:kamp_id',function(req,res,next){
    Kamp.findById(req.params.kamp_id, function(err, kamp){
      if (err)
          res.send(err);
      if(req.body.naam)
        kamp.naam = req.body.naam;
      if(req.body.omschrijving)
        kamp.omschrijving = req.body.omschrijving;
      if(req.body.startDatum)
        kamp.startDatum = req.body.startDatum;
      if(req.body.naam)
        kamp.eindDatum = req.body.eindDatum;
      if(req.body.naam)
        kamp.aantalDagen = req.body.aantalDagen;
      if(req.body.vervoer)
        kamp.vervoer = req.body.vervoer;
      if(req.body.formule)
        kamp.formule = req.body.formule;
      if(req.body.basisPrijs)
        kamp.basisPrijs = req.body.basisPrijs;
      if(req.body.bondPrijs)
        kamp.bondPrijs = req.body.bondPrijs;
      if(req.body.kortingen)
        kamp.kortingen = req.body.kortingen;
      if(req.body.inbegrepenInPrijs)
        kamp.inbegrepenInPrijs = req.body.inbegrepenInPrijs;
      if(req.body.minLeeftijd)
        kamp.minLeeftijd = req.body.minLeeftijd;
      if(req.body.maxLeeftijd)
        kamp.maxLeeftijd = req.body.maxLeeftijd;
      if(req.body.maxDeelnemers)
        kamp.maxDeelnemers = req.body.maxDeelnemers;
      if(req.body.contract)
        kamp.contact = req.body.contact;

      if(req.files.sfeerfoto){
        var sfeerfoto = req.files.sfeerfoto.path;
        sfeerfoto = sfeerfoto.replace("public", ".");
        kamp.sfeerfoto = sfeerfoto;
      }
      kamp.save(function(err, kamp){
        if(err){ return next(err); }
          return res.json(kamp);
    	});
    });
});

router.put('/adres', function(req, res, next){
  var data = req.body;
  Adres.findById(data._id, function(err, adres) {
    adres.straat = data.straat;
    adres.huisnummer = data.huisnummer;
    adres.bus = data.bus;
    adres.gemeente = data.gemeente;
    adres.postcode = data.postcode;
    adres.save(function(err, adres){
      if(err){console.log(error); return next(err); }
        return res.json(adres);
    });
  });
})
//Users

router.post('/registerMonitor', function(req, res, next){
  if(!req.body.email || !req.body.password){
    return res.status(400).json({message: 'Gelieve alle velden in te vullen'});
  }
  var monitor = new Monitor();
  monitor.email = req.body.email;
  monitor.setPassword(req.body.password);
  monitor.naam = req.body.naam;
  monitor.voornaam = req.body.voornaam;
  monitor.rijksregisternummer = req.body.rijksregisternummer;

  monitor.save(function (err, monitor){
    if(err){return next(err); }

    return res.json(monitor);
  });
});

router.put('/monitoraanpassen', function(req, res, next){
  Monitor.findById(req.body._id, function(err, monitor) {
      monitor.naam = req.body.naam;
      monitor.voornaam = req.body.voornaam;
      monitor.rijksregisternummer = req.body.rijksregisternummer;
      monitor.save(function (err){
        if(err){return next(err); }
        var data = req.body.adres;
        Adres.findById(data._id, function(err, adres) {
          adres.straat = data.straat;
          adres.huisnummer = data.huisnummer;
          adres.bus = data.bus;
          adres.gemeente = data.gemeente;
          adres.postcode = data.postcode;
          adres.save(function(err, adres){
            if(err){console.log(error); return next(err); }
              return res.json({token: monitor.generateJWT()})
          });
        });
      });
  });
});


router.post('/registerLid', function(req, res, next){
  if(!req.body.email || !req.body.password || !req.body.bevestigpassword || !req.body.naam || !req.body.voornaam || !req.body.rijksregisternummer || !req.body.geboorteDatum || !req.body.codeGerechtigde){
    return res.status(400).json({message: 'Gelieve alle velden in te vullen aub'});
  }
  if(req.body.password != req.body.bevestigpassword){
    return res.status(400).json({message: 'Gelieve 2 maal het zelfde passwoord in te geven'});
  }
  var lid = new Lid();
  lid.email = req.body.email;
  lid.setPassword(req.body.password);
  lid.naam = req.body.naam;
  lid.voornaam = req.body.voornaam;
  lid.rijksregisternummer = req.body.rijksregisternummer;
  lid.geboorteDatum = req.body.geboorteDatum;
  lid.codeGerechtigde = req.body.codeGerechtigde;
  lid.save(function (err){
    if(err){return next(err); }

    return res.json({token: lid.generateJWT()})
  });

});

router.put('/lidaanpassen', function(req, res, next){
  Lid.findById(req.body._id, function(err, lid) {
      lid.naam = req.body.naam;
      lid.voornaam = req.body.voornaam;
      lid.rijksregisternummer = req.body.rijksregisternummer;
      lid.geboorteDatum = req.body.geboorteDatum;
      lid.codeGerechtigde = req.body.codeGerechtigde;
      lid.save(function (err){
        if(err){console.log(error); return next(err); }
        var data = req.body.adres;
        Adres.findById(data._id, function(err, adres) {
          adres.straat = data.straat;
          adres.huisnummer = data.huisnummer;
          adres.bus = data.bus;
          adres.gemeente = data.gemeente;
          adres.postcode = data.postcode;
          adres.save(function(err, adres){
            if(err){console.log(error); return next(err); }
              return res.json({token: lid.generateJWT()})
          });
      });
    });
    });
});

router.post('/registerBeheerder', function(req, res, next){
  if(!req.body.email || !req.body.password){
    return res.status(400).json({message: 'Gelieve beide velden in te vullen aub!'});
  }

  var beheerder = new Beheerder();
  beheerder.email = req.body.email;
  beheerder.setPassword(req.body.password);
  //monitor.naam = req.body.naam;
  //monitor.voornaam = req.body.voornaam;
  //monitor.rijksregisternummer = req.body.rijksregisternummer;

  beheerder.save(function (err){
    if(err){return next(err); }

    return res.json({token: beheerder.generateJWT()})
  });
});

router.put('/beheerderAanpassen', function(req, res, next){
    Beheerder.findById(req.body._id, function(err, beheerder) {
      beheerder.naam = req.body.naam;
      beheerder.voornaam = req.body.voornaam;
      beheerder.rijksregisternummer = req.body.rijksregisternummer;
      beheerder.save(function (err){
        if(err){return next(err); }
        var data = req.body.adres;
        Adres.findById(data._id, function(err, adres) {
          adres.straat = data.straat;
          adres.huisnummer = data.huisnummer;
          adres.bus = data.bus;
          adres.gemeente = data.gemeente;
          adres.postcode = data.postcode;
          adres.save(function(err, adres){
            if(err){console.log(error); return next(err); }
              return res.json({token: beheerder.generateJWT()})
          });
        });
      });
  });
});

router.post('/login', function(req, res, next){

  if(!req.body.email || !req.body.password){
    return res.status(400).json({message: 'Gelieve beide velden in te vullen!'});
  }
  passport.authenticate('local-login', function(err, user, info){
    if(err){ return next(err); }

    if(user){
      return res.json({token: user.generateJWT()});
    } else {
      return res.status(401).json(info);
    }
  })(req, res, next);
});

router.get('/users', function(req, res, next) {
  User.find(function(err, users){
    if(err){ return next(err); }

    res.json(users);
  });
});

router.param('lid', function(req, res, next, id) {
  var query = Lid.findById(id);

  query.exec(function (err, lid){
    if (err) { return next(err); }
    if (!lid) { return next(new Error("can geen lid vinden")); }

    req.lid = lid;
    return next();
  });
});


router.get('/leden/:lid', function(req, res, next){
  var id = req.lid._id;
  Lid.findOne({ _id: id })
  .populate([{path:'adres'}, {path:'inschrijvingen'}, {path:'contactpersonen'}])
  .exec (function (err, lid) {
  if (err){
    console.log(err);
  }
  else{
    req.lid = lid;
    res.json(req.lid);
  }
})

});

router.post('/leden/:lid/contactpersoon', function(req, res, next){
  var contactpersoon = new Contactpersoon(req.body);
  contactpersoon.leden.push(req.lid);
  contactpersoon.save(function(err, contactpersoon){
    if(err){ return next(err); }
    Lid.findById(req.lid._id, function(err, lid) {
      lid.contactpersonen.push(contactpersoon);
      lid.save(function(err, reslid){
        if(err){ return next(err); }
        return res.json(contactpersoon);
      })
    })
  })
});

router.post('/contactpersoon/:contactpersoon/adres', function(req, res, next){
  var adres = new Adres(req.body);
  var contactpersoon = req.contactpersoon;
  adres.contactpersonen.push(contactpersoon);
  adres.save(function(err, adres){
    if(err){ console.log(err); return next(err); }
    contactpersoon.adres = adres;
    contactpersoon.save(function(err, contactpersoon){
      if(err){ return next(err); }
      return res.json(adres);
    })
  })
});

router.param('contactpersoon', function(req, res, next, id) {
  var query = Contactpersoon.findById(id);

  query.exec(function (err, contactpersoon){
    if (err) { return next(err); }
    if (!contactpersoon) { return next(new Error("can geen contactpersoon vinden")); }

    req.contactpersoon = contactpersoon;
    return next();
  });
});

router.put('/leden/:lid/contactpersoon', function(req, res, next){
  console.log(req.body);
  Contactpersoon.findById(req.body._id, function(err, contactpersoon) {
    console.log('contactpersoon findby');
    contactpersoon.naam = req.body.naam;
    contactpersoon.voornaam = req.body.voornaam;
    contactpersoon.rijksregisternummer = req.body.rijksregisternummer;
    contactpersoon.telefoonnummer = req.body.telefoonnummer;
    contactpersoon.email = req.body.email;
    contactpersoon.aansluitnummer = req.body.aansluitnummer;
    contactpersoon.betalend = req.body.betalend;
    contactpersoon.ouder = req.body.ouder;
    contactpersoon.save(function (err){
      if(err){return next(err); }
      var data = req.body.adres;
      Adres.findById(data._id, function(err, adres) {
        console.log('adres findby');
        adres.straat = data.straat;
        adres.huisnummer = data.huisnummer;
        adres.bus = data.bus;
        adres.gemeente = data.gemeente;
        adres.postcode = data.postcode;
        adres.save(function(err, adres){
          if(err){console.log(error); return next(err); }
            return res.json(contactpersoon);
        });
      });
    });
  });
});

router.get('/contactpersoon/:contactpersoon', function(req, res, next){
    var id = req.contactpersoon._id;
    Contactpersoon.findOne({ _id: id })
    .populate([{path:'lid'}, {path:'adres'}])
    .exec (function (err, contactpersoon) {
    if (err){
      console.log(err);
    }
    else{
      req.contactpersoon = contactpersoon;
      res.json(req.contactpersoon);
    }
  })

});


router.post('/leden/:lid/adres', function(req, res, next) {
  var adres = new Adres(req.body);
  adres.leden.push(req.lid);

  adres.save(function(err, adres){
    if(err){ return next(err); }

    req.lid.adres = adres;
    req.lid.save(function(err, lid) {
      if(err){ return next(err); }

      res.json(lid);
    });
  });
});

router.post('/leden/:lid/inschrijven', function(req, res, next){
  var inschrijving = new Inschrijving(req.body);
  var kamp = req.body.kamp;
  console.log(kamp);
  inschrijving.lid = req.lid;
  console.log('1');
  inschrijving.save(function(err, inschrijving){
    if(err){ return next(err); }
    console.log('1');
    Lid.findById(req.lid._id, function(err, lid) {
      console.log('1');
      lid.inschrijvingen.push(inschrijving);
      console.log('1');
      lid.save(function(err, lid){
        if(err){ return next(err); }
        console.log('1');
        Kamp.findById(kamp._id, function(err, kamp) {
          console.log('1');
          kamp.inschrijvingen.push(inschrijving);
          console.log('1');
          kamp.save(function(err, lid){
            if(err){ return next(err); }
            return res.json(inschrijving);
          });
        });
      });
    });
  });
});

router.get('/inschrijvingen/lid/:lid', function(req, res, next){
    var id = req.lid._id;
    Inschrijving.where({ lid: id })
    .populate([{path:'lid'}, {path:'kamp'}])
    .exec (function (err, inschrijvingen) {
    if (err){
      console.log(err);
    }
    else{
      res.json(inschrijvingen);
    }
  })

});

router.get('/inschrijving/:inschrijving', function(req, res, next){
    var id = req.inschrijving._id;
    Inschrijving.findOne({ _id: id })
    .populate([{path:'lid'}, {path:'kamp'}])
    .exec (function (err, inschrijving) {
    if (err){
      console.log(err);
    }
    else{
      Lid.findOne({ _id: inschrijving.lid._id })
        .populate([{path:'adres'}, {path:'inschrijvingen'}, {path:'contactpersonen'}])
        .exec (function (err, lid) {
        if (err){
          console.log(err);
        }
        else{
          inschrijving.lid = lid;
          res.json(inschrijving);
        }
      })

    }
  })

});

router.get('/inschrijvingen/kamp/:kamp', function(req, res, next){
    var id = req.kamp._id;
    Inschrijving.where({ kamp: id })
    .populate([{path:'lid'}, {path:'kamp'}])
    .exec (function (err, inschrijvingen) {
    if (err){
      console.log(err);
    }
    else{
      res.json(inschrijvingen);
    }
  })

});


router.param('inschrijving', function(req, res, next, id) {
  var query = Inschrijving.findById(id);

  query.exec(function (err, inschrijving){
    if (err) { return next(err); }
    if (!inschrijving) { return next(new Error("can geen inschrijving vinden")); }

    req.inschrijving = inschrijving;
    return next();
  });
});


router.put('/inschrijvingen/:inschrijving', function(req, res, next){
  Inschrijving.findById(req.inschrijving._id, function(err, inschrijving) {
    inschrijving.extraInformatie = req.body.extraInformatie;
    inschrijving.goedgekeurd = req.body.goedgekeurd;
    inschrijving.betaald = req.body.betaald;
    inschrijving.save(function(err, inschrijving){
      if(err){ return next(err); }
      return res.json(inschrijving);
    });
  });
});


router.param('monitor', function(req, res, next, id) {
  var query = Monitor.findById(id);

  query.exec(function (err, monitor){
    if (err) { return next(err); }
    if (!monitor) { return next(new Error("can geen monitor vinden")); }

    req.monitor = monitor;
    return next();
  });
});

router.get('/monitoren/:monitor', function(req, res, next){
    var id = req.monitor._id;
    Monitor.findOne({ _id: id })
    .populate([{path:'adres'}, {path:'activiteiten'}, {path:'kamp'}])
    .exec (function (err, monitor) {
    if (err){
      console.log(err);
    }
    else{
      res.json(monitor);
    }
  })
});

router.post('/monitoren/:monitor/adres', function(req, res, next) {
  var adres = new Adres(req.body);
  adres.monitoren.push(req.monitor);
  adres.save(function(err, adres){
    if(err){ return next(err); }
    req.monitor.adres = adres;
    req.monitor.save(function(err, monitor) {
      if(err){ return next(err); }

      res.json(monitor);
    });
  });
});

router.put('/monitoren/:monitor/kamp', function(req, res, next){
  var kamp = req.body;
  Monitor.findById(req.monitor._id, function(err, monitor) {
    monitor.kampen.push(kamp);
    monitor.save(function(err, monitor){
      if(err){ return next(err); }
      Kamp.findById(kamp._id, function(err, kamp){
        kamp.monitoren.push(monitor);
        kamp.save(function(err, kamp){
          if(err){ return next(err); }
          return res.json(kamp);
        });
      });
    });
  });
});

router.param('beheerder', function(req, res, next, id) {
  var query = Beheerder.findById(id);
  query.exec(function (err, beheerder){
    if (err) { return next(err); }
    if (!beheerder) { return next(new Error("can geen lid vinden")); }

    req.beheerder = beheerder;
    return next();
  });
});

router.get('/beheerders/:beheerder', function(req, res, next){
    var id = req.beheerder._id;
    Beheerder.findOne({ _id: id })
    .populate([{path:'adres'}, {path:'activiteiten'}, {path:'kamp'}])
    .exec (function (err, beheerder) {
    if (err){
      console.log(err);
    }
    else{
      req.beheerder = beheerder;
      res.json(req.beheerder);
    }
  })
});

router.post('/beheerders/:beheerder/adres', function(req, res, next) {
  var adres = new Adres(req.body);
  adres.beheerders.push(req.beheerder);

  adres.save(function(err, adres){
    if(err){ return next(err); }

    req.beheerder.adres = adres;
    req.beheerder.save(function(err, beheerder) {
      if(err){ return next(err); }

      res.json(beheerder);
    });
  });
});

router.post('/monitoren/:monitor/activiteit', function(req, res, next) {
  var activiteit = new Activiteit(req.body);
  var kamp = req.body.kamp;
  Monitor.findById(req.monitor._id, function(err, monitor) {
    monitor.activiteiten.push(activiteit);
    monitor.save(function(err, monitor){
      if(err){ return next(err); }
      Kamp.findById(kamp._id, function(err, kamp) {
        kamp.activiteiten.push(activiteit);
        kamp.save(function(err, kamp){
          if(err){ return next(err); }
          activiteit.monitoren.push(req.monitor);
          activiteit.save(function(err, activiteit){
            if(err){ return next(err); }
            res.json(activiteit);
          });
        });
      });
    });
  });
});

router.param('activiteit', function(req, res, next, id) {
  var query = Activiteit.findById(id);
  query.exec(function (err, activiteit){

    if (err) {console.log(err); return next(err); }
    if (!activiteit) { return next(new Error("can geen activiteit vinden")); }
    req.activiteit = activiteit;
    return next();
  });
});

router.get('/activiteit/:activiteit', function(req, res, next){
  var id = req.activiteit._id;
  Activiteit.findOne({ _id: id })
  .populate([{path:'beheerders'}, {path:'monitoren'}, {path:'kamp'}])
  .exec (function (err, activiteit) {
  if (err){
    console.log(err);
  }
  else{
    res.json(activiteit);
  }
})
});

router.put('/activiteit/:activiteit', function(req, res, next){
  Activiteit.findById(req.activiteit._id, function(err, activiteit) {
    activiteit.naam = req.body.naam;
    activiteit.locatie = req.body.locatie;
    activiteit.heleDag = req.body.heleDag;
    activiteit.begin = req.body.begin;
    activiteit.einde = req.body.einde;
    activiteit.save(function(err, activiteit){
      if(err){ return next(err); }
      res.json(activiteit);
    });
  });
});

module.exports = router;
